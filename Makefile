VERSION				:= 0.2.0

sysconfdir		?= /etc/mkplatform
datarootdir		?= /usr/share
datadir				?= $(datarootdir)/mkplatform

SBIN_FILES		:= mkplatform \
							apk-extract
SHARE_FILES		:= platform-init \
							platform-update \
							init.d/bootconf \
							init.d/zfs \
							features.d/platform.files \
							features.d/platform.modules \
							mkplatform.conf \
							mkinitfs.conf \
							functions/common.sh \
							functions/make-image.sh

SCRIPTS		:= mkplatform platform-init platform-update apk-extract
IN_FILES	:= $(addsuffix .in,$(SCRIPTS))


GIT_REV := $(shell test -d .git && git describe || echo exported)
ifneq ($(GIT_REV), exported)
FULL_VERSION    := $(patsubst $(PACKAGE)-%,%,$(GIT_REV))
FULL_VERSION    := $(patsubst v%,%,$(FULL_VERSION))
else
FULL_VERSION    := $(VERSION)
endif


INSTALL		:= install
SED		:= sed
SED_REPLACE	:= -e 's:@VERSION@:$(FULL_VERSION):g' \
		-e 's:@sysconfdir@:$(sysconfdir):g' \
		-e 's:@datadir@:$(datadir):g'

all:	$(SBIN_FILES) $(SCRIPTS)

clean:
	rm -f $(SCRIPTS)

.SUFFIXES:	.in
.in:
	${SED} ${SED_REPLACE} ${SED_EXTRA} $< > $@

install: $(SBIN_FILES) $(SHARE_FILES)
	for i in $(SBIN_FILES); do \
		$(INSTALL) -Dm755 $$i $(DESTDIR)/sbin/$$i;\
	done
	for i in $(SHARE_FILES); do \
		$(INSTALL) -D $$i $(DESTDIR)/$(datadir)/$$i;\
	done
