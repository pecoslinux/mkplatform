#Functions shared across mkplatform scripts


#Send message to stderr and exit with an error code
die() {
  local message=$1
  echo "$message" 1>&2
  exit 1

}

has_key() {
  local key="$1"
  local file="$2"
  #Look for the key at the beginning of the line with a whitespace
  #exit after the first match
  grep -q -m 1 "^${key}\s" $file
}

#Get a single key by name from a platformconf file
get_key() {
  local key="$1"
  local file="$2"
  [ -r "$file" ] || die "Error getting key $key, could not read conf file $file"
  awk -v key="$key" '
    $1 == key {
        #remove key name
        $1=""
        #remove leading space
        gsub(/^[ ]*/, "")
        #remove trailing space
        gsub(/[ ]*$/, "")
        #remove quotes
        gsub(/['"'"'"]/, "")
        #print value
        print
        #return after first match
        exit
      }
  ' "$file"
}

#Get all valid keys and return an eval-able string
get_keys() {
  local key="$1"
  local file="$2"
  [ -r "$file" ] || die "Error getting keys $key, could not read conf file $file"
  awk -v key="$key" '
    $1 == key {
        #remove key name
        $1=""
        #remove leading space
        gsub(/^[ ]*/, "")
        #remove trailing space
        gsub(/[ ]*$/, "")
        #remove quotes
        gsub(/['"'"'"]/, "")
        #print value
        print
      }
  ' "$file"
}

#Get a block of text from a configuration file
get_block() {
  local block="$1" #Name of the block
  local value="$2" #Unique value identifying this block
  local file="$3"
  [ -r "$file" ] || die "Error getting block $block, could not read conf file '$file'"
  awk -v block="$block" -v value="$value" '
  BEGIN { in_file = 0 }
    $0 ~ "^" block "\\s+" { if (value == $2) in_block = 1; next }
    in_block && $1 == block"END" {
      exit
    }
    in_block {
      print
      next
    }
  END {
    if (in_block == 1)
      exit 0
    else
      exit 1
    }
  ' "$file"
}



#Add an Openrc init script to start on a run level
rc_add() {
  local prefix=$1
  local rcscript=$2
  local runlevel=$3
  mkdir -p $prefix/etc/runlevels/$runlevel
  ln -fs /etc/init.d/$rcscript $prefix/etc/runlevels/$runlevel/$rcscript
}

#Convert a string to lower case
to_lower() {
    echo "$1" | tr [:upper:] [:lower:]
}

#Generate a VERSION_ID based on the date
new_version_id() {
  echo "$(date -Iseconds | sed 's/+\d*//' | tr -d '[:]-')Z"
}

#Locate the kernel version packed inside an initrd and dump to stdout
dump_kernel_version_from_initrd() {
  local initrd="$1"
  local flavor="$2"
  [ -z "$flavor" ] && flavor="vanilla"
  zcat $initrd | cpio -it 2>/dev/null | grep -e "^lib/modules/.*-${flavor}$" \
    -m 1 | cut -d'/' -f3

}

#Dump the kernel version number embedded inot a kernel image file
dump_kernel_version_from_kernel() {
  local kernel="$1"
  local flavor="$2"
  [ -z "$flavor" ] && flavor="vanilla"
  file $kernel | grep -o "[.0-9\-]*$flavor"
}

#Read a response from the console, but don't echo it
read_hidden_response() {
  set -o noglob
  stty -echo
  read -r response
  stty echo
  set +o noglob
  echo
}

#Read a response from the console
read_normal_response() {
  read response
}

#Get a response from user, with optoion prompt and default values
get_response() {
  local type=$1
  local prompt=$2
  local default=$3
  response=
  case $type in
    password)
      local _original_ifs=$IFS
      IFS=
      while true; do
        echo -n "Password: "
        read_hidden_response
        local _confirm=$response
        echo -n "Re-type password: "
        read_hidden_response
        [ "$response" = "$_confirm" ] && break
        echo "Passwords do not match, try again."
      done
      IFS=$_original_ifs
      ;;
    prompt)
      while true; do
        echo -n "$prompt "
        [ -n "$default" ] && echo -n "[$default] "
        read_normal_response
        [ -n "$default" ] && : ${response:=$default}
        [ -n "$response" ] && break
      done
      ;;
    yesno)
      while true; do
        echo -n "$prompt "
        [ -n "$default" ] && echo -n "[$default] "
        read_normal_response
        case $response in
          [Yy]|[Yy][Ee][Ss]|1|[Tt][Rr][Uu][Ee])
            response='yes'
            ;;
          [Nn]|[Nn][Oo]|0|[Fa][Aa][Ll][Ss][Ee])
            response='no'
            ;;
          *)
            if ([ -z "$response" ] && [ -n "$default" ]); then
              response="$default"
            else
              response=
            fi
            ;;
        esac
        [ -n "$response" ] && break
      done
      ;;


  esac
}

