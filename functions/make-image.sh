#Code and ideas from linuxkit/tools/make-bios

make_img() {

  local esp_file=$(mktemp -t -u esp-XXXXXX)
  local img=$(mktemp -t platform-image-XXXXXX)
  local bootconf=$(mktemp -t bootconf-XXXXXX)
  local syslinuxcfg=$(mktemp -t syslinuxcfg-XXXXXX)

  ( exec 1>&2;

  if [ ! -t 0 ]; then
    cat > $bootconf
  else
    [ -r "$BOOTCONF" ] || die "Could not read boot configuration file $BOOTCONF"
    cp $BOOTCONF $bootconf
  fi

  [ -r "$KERNEL" ] || die "Could not read kernel $KERNEL"

  [ -r "$INITRD" ] || die "Could not read initrd $INITRD"

  [ -r "$PLATFORMCONF" ] || die "Could not read platform configuration file $PLATFORMCONF"

  local name=$(get_key NAME $PLATFORMCONF)
  local version=$(get_key NAME $PLATFORMCONF)
  local kver=$(dump_kernel_version_from_kernel $KERNEL)

  echo "Making disk image"

  make_syslinuxcfg $name $version $kver > $syslinuxcfg

  #Get size of kernel and initrd files
  local kernel_file_size=$(stat -c %s "$KERNEL")
  local initrd_file_size=$(stat -c %s "$INITRD")

  #Calculate minimum headroom needed for ESP file in bytes
  #For example 1024*1024=1MB (512KB seems to be a sufficient minimum)
  local esp_headroom=$(( 1024 * 1024 ))

  #Calculate total size of ESP file in bytes
  #TODO: Get SIZE parameter from platform.conf and if larger, use it instead
  local scale=3 #Scaling factor to allow for multple kernels and initrd files for updates
  local esp_file_size=$(( $scale * ( $kernel_file_size + $initrd_file_size + $esp_headroom )))

  # (x+1024)/1024*1024 rounds up to multiple of 1024KB, or 2048 sectors
  # some firmwares get confused if the partitions are not aligned on 2048 blocks
  # we will round up to the nearest multiple of 2048 blocks
  # since each block is 512 bytes, we want the size to be a multiple of
  # 2048 blocks * 512 bytes = 1048576 bytes = 1024KB
  local esp_file_size_kb=$(( ( ( ($esp_file_size+1024-1) / 1024 ) + 1024-1) / 1024 * 1024 ))
  # and for sectors
  local esp_file_size_sectors=$(( $esp_file_size_kb * 2 ))

  # create a raw disk with an EFI boot partition
  # Stuff it into a FAT filesystem, making it as small as possible.
  mkfs.vfat -v -C $esp_file $(( esp_file_size_kb )) > /dev/null

  mcopy -i $esp_file $bootconf ::boot.conf
  mmd -i $esp_file ::boot
  mcopy -i $esp_file $KERNEL ::boot/linux
  mcopy -i $esp_file $INITRD ::boot/platform
  mmd -i $esp_file ::boot/syslinux

  local fn
  for fn in isohdpfx.bin isolinux.bin ldlinux.c32 libutil.c32 libcom32.c32 mboot.c32 menu.c32; do
    mcopy -i $esp_file /usr/share/syslinux/$fn ::boot/syslinux/$fn
  done

  mcopy -i $esp_file $syslinuxcfg ::/boot/syslinux/syslinux.cfg

  syslinux --directory /boot/syslinux $esp_file

  # now make our actual filesystem image
  # how big an image do we want?
  # it should be the size of our ESP file+1MB for BIOS boot + 1MB for MBR + 1MB for backup
  local onemb=$(( 1024 * 1024 ))
  local size_in_bytes=$(( $(stat -c %s "$esp_file") + 4*$onemb ))

  # and make sure the ESP is bootable for BIOS mode
  # settings
  local blksize=512
  local mb_blocks=$(( $size_in_bytes / $onemb ))

  #Make the image file
  dd if=/dev/zero of=$img bs=1M count=$mb_blocks &>/dev/null

  #Use fdisk to wipe partition signatures
  echo "w" | fdisk $img &>/dev/null || true

  #Format one large partition of the apprropriate size
  echo ","$esp_file_size_sectors",;" | sfdisk $img &>/dev/null

  # of course, "one large partition" means minus the 2048 at the beginning
  local esp_sector_start=2048

  #Copy EFI System Partition image into disk image file
  dd if=$esp_file of=$img bs=$blksize count=$esp_file_size_sectors \
    conv=notrunc seek=$esp_sector_start &>/dev/null

  #Install mbr
  dd if=/usr/share/syslinux/altmbr.bin bs=439 count=1 conv=notrunc of=$img &>/dev/null

  printf '\1' | dd bs=1 count=1 seek=439 conv=notrunc of=$img &>/dev/null

  #Set the first partition as Active/bootable
  sfdisk -A "$img" 1 &>/dev/null

  rm $esp_file
  rm $bootconf
  rm $syslinuxcfg

  )

  cat $img

  [ $? -eq 0 ] || exit 1

  if [ -z "$OUTPUT" ]; then
    cat $img
    rm -f $img
  else
    mv $img $OUTPUT
  fi

}

